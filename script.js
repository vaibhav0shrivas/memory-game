const gameContainer = document.getElementById("game");
const instructionsContainer = document.getElementById("instructions");
const gameDiffculty = [
  4,
  6,
  12
];

let previouslyClickedElement = null;
let cardMatchesRemaining = -1;
let startTime = null;
let endTime = null;
let timeForThisPlay = null;
let clickingIsAllowed = null;
let clickCounter = null;
let diffcultyChangable = null;
let diffcultyLevel = 1; //0 or 1 or 2
let confirmationGivenForReset = null;



const CARDS = [
  "card-1",
  "card-2",
  "card-3",
  "card-4",
  "card-5",
  "card-6",
  "card-7",
  "card-8",
  "card-9",
  "card-10",
  "card-11",
  "card-12"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(passedArray) {
  let array = [].concat(passedArray, passedArray);
  let counter = array.length;
  cardMatchesRemaining = array.length / 2;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledCARDS;

function createDivsForCARDS(cardArray) {

  const mediaQuery = window.matchMedia('(max-width: 640px)');

  for (let card of cardArray) {

    const newDiv = document.createElement("div");

    newDiv.setAttribute("data-card-value", card);
    newDiv.className = "facedown";

    newDiv.addEventListener("click", handleCardClick);

    if (cardArray.length == 24) {

      newDiv.style.flexBasis = "13.5vw";
      newDiv.style.height = "13.5vh";

    }

    if (cardArray.length == 8) {

      newDiv.style.flexBasis = "35vw";
      newDiv.style.height = "13.5vh";

    }

    if (mediaQuery.matches) {

      if (cardArray.length == 12) {

        newDiv.style.flexBasis = "24vw";
        newDiv.style.height = "14vh";

      }

      if (cardArray.length == 24) {

        newDiv.style.flexBasis = "8vw";
        newDiv.style.height = "10.5vh";

      }

      if (cardArray.length == 8) {

        newDiv.style.flexBasis = "35vw";
        newDiv.style.height = "20vh";

      }

    }

    gameContainer.append(newDiv);

  }

}

document.getElementById("difficulty").addEventListener("click", (event) => {

  if (diffcultyChangable == false) {
    return;
  } else {

    diffcultyLevel++;
    document.getElementById("difficulty").textContent = `${gameDiffculty[diffcultyLevel % 3] * 2} Cards`;
    diffcultyLevel %= 3;
    removeDivsForGameArea();
    shuffledCARDS = shuffle(CARDS.slice(CARDS.length - gameDiffculty[diffcultyLevel % 3]));
    createDivsForCARDS(shuffledCARDS);
    clickingIsAllowed = false;

  }

})

document.getElementById("try-again").addEventListener("click", (event) => {

  event.target.textContent = "Show Instructions and Reset Game";

  shuffledCARDS = shuffle(CARDS.slice(CARDS.length - gameDiffculty[diffcultyLevel % 3]));
  removeDivsForGameArea();
  createDivsForCARDS(shuffledCARDS);
  document.getElementById("play").textContent = "Show Instructions and Reset Game";
  document.getElementById("play").style.display = "block";
  updateBestTime();
  updateBestScore(clickCounter);
  reRenderHighSocres();
  toggleGameArea("flex");
  diffcultyChangable = false;
  toggleInstructionsArea("none");
  toggleGameEndedArea("none");
  toggleCurrentClicksDiv("block");

  previouslyClickedElement = null;
  clickingIsAllowed = true;
  confirmationGivenForReset = null;
  clickCounter = 0;

  updateCurrentClicks();

  startTime = new Date();

})



document.getElementById("play").addEventListener("click", (event) => {

  if (event.target.textContent == "Play") {

    event.target.textContent = "Show Instructions and Reset Game";
    event.target.setAttribute("title", "Click to Reset the game");
    toggleCurrentClicksDiv("block");
    diffcultyChangable = false;
    clickingIsAllowed = true;


    shuffledCARDS = shuffle(CARDS.slice(CARDS.length - gameDiffculty[diffcultyLevel % 3]));
    removeDivsForGameArea();
    createDivsForCARDS(shuffledCARDS);

    previouslyClickedElement = null;
    clickCounter = 0;

    startTime = new Date();
    toggleInstructionsArea("none");

  } else {

    toggleConfirmResetMessageArea("flex");

  }

});

document.getElementById("confirm-reset-yes").addEventListener("click", (event) => {
  confirmationGivenForReset = true;
  afterConfirmation(confirmationGivenForReset);

});

document.getElementById("confirm-reset-no").addEventListener("click", (event) => {
  confirmationGivenForReset = false;
  afterConfirmation(confirmationGivenForReset);

});

window.onload = (event) => {

  diffcultyChangable = true;
  clickingIsAllowed = false;
  confirmationGivenForReset = null;
  shuffledCARDS = shuffle(CARDS.slice(CARDS.length - gameDiffculty[diffcultyLevel % 3]));
  removeDivsForGameArea();
  createDivsForCARDS(shuffledCARDS);
  toggleGameArea("flex");
  reRenderHighSocres();


};

// TODO: Implement this function!
function handleCardClick(event) {

  if (event.target === previouslyClickedElement || clickingIsAllowed === false
    || document.getElementById("confirm-reset").style.display === "flex") {

    return;

  }

  event.target.classList.remove("facedown");
  event.target.classList.add(event.target.getAttribute("data-card-value"));
  clickCounter++;
  updateCurrentClicks();


  if (previouslyClickedElement === null) {

    previouslyClickedElement = event.target;
    return;

  } else {

    if (previouslyClickedElement.getAttribute("data-card-value") != event.target.getAttribute("data-card-value")) {

      clickingIsAllowed = false;

      setTimeout(() => {
        event.target.classList.remove(event.target.getAttribute("data-card-value"));
        event.target.classList.add("facedown");

        previouslyClickedElement.classList.remove(previouslyClickedElement.getAttribute("data-card-value"));
        previouslyClickedElement.classList.add("facedown");

        previouslyClickedElement = null;
        clickingIsAllowed = true;

      },
        1 * 1000);

    } else {

      cardMatchesRemaining--;

      event.target.removeEventListener("click", handleCardClick);
      previouslyClickedElement.removeEventListener("click", handleCardClick);

      previouslyClickedElement = null;
      clickingIsAllowed = true;

      if (cardMatchesRemaining == 0) {
        endTime = new Date();
        displayGameEndPrompt();
      }
    }
  }
}



function toggleGameEndedArea(display = null) {

  let gameEndedContainer = document.getElementById("game-complete");
  if (display != null) {
    gameEndedContainer.style.display = display;
    return;
  }

  if (gameEndedContainer.style.display === "none") {
    gameEndedContainer.style.display = "flex";
  } else {
    gameEndedContainer.style.display = "none";
  }

}


function toggleInstructionsArea(display = null) {

  if (display != null) {
    instructionsContainer.style.display = display;
    return;
  }

  if (instructionsContainer.style.display === "none") {
    instructionsContainer.style.display = "flex";
  } else {
    instructionsContainer.style.display = "none";
  }

}

function toggleCurrentClicksDiv(display = null) {

  document.getElementById("current-score").textContent = "";

  if (display != null) {
    document.getElementById("current-score").style.display = display;
    return;
  }

  if (document.getElementById("current-score").style.display === "none") {
    document.getElementById("current-score").style.display = "block";
  } else {
    document.getElementById("current-score").style.display = "none";
  }

}

function toggleDifficultyButton(display = null) {

  document.getElementById("difficulty").textContent = `${gameDiffculty[diffcultyLevel % 3]} Cards`;

  if (display != null) {
    document.getElementById("difficulty").style.display = display;
    return;
  }

  if (document.getElementById("difficulty").style.display === "none") {
    document.getElementById("difficulty").style.display = "block";
  } else {
    document.getElementById("difficulty").style.display = "none";
  }

}

function toggleGameArea(display = null) {

  if (display != null) {
    gameContainer.style.display = display;
    return;
  }

  if (gameContainer.style.display === "none") {
    gameContainer.style.display = "flex";
    diffcultyChangable = false;
  } else {
    gameContainer.style.display = "none";
    diffcultyChangable = true;
  }

}


function toggleConfirmResetMessageArea(display = null) {

  let container = document.getElementById("confirm-reset");
  if (display != null) {
    container.style.display = display;
    return;
  }

  if (container.style.display === "none") {
    container.style.display = "flex";

  } else {
    container.style.display = "none";

  }

}

function displayGameEndPrompt() {

  clickingIsAllowed = false;
  updateBestTime();
  updateBestScore(clickCounter);
  toggleInstructionsArea("none");
  document.getElementById("play").style.display = "none";
  toggleGameArea("none");
  diffcultyChangable = true;

  document.getElementById("game-complete").style.display = "flex";
  document.getElementById("win-message").textContent = `You took ${clickCounter} clicks and ${timeForThisPlay / 1000} seconds`;

}

function removeDivsForGameArea() {

  if (gameContainer.childElementCount > 0) {
    gameContainer.textContent = ""
  }
}



function getBestTimeFromLocalStorage() {

  if (localStorage.getItem('bestTime') !== null && localStorage.getItem('bestTime').startsWith("-") == false) {

    let bestTimeNumber = Number.parseInt(localStorage.getItem('bestTime'));
    if (bestTimeNumber > 0) {
      return bestTimeNumber;
    }
    return "--";

  }
  return "--";

}

function getBestScoreFromLocalStorage() {

  if (localStorage.getItem('bestScore') !== null) {

    let bestScoreNumber = Number.parseInt(localStorage.getItem('bestScore'));
    if (bestScoreNumber > 0) {
      return bestScoreNumber;
    }
    return "--";

  }
  return "--";

}

function updateBestTime() {

  if (endTime == null || startTime == null) {
    return;
  }

  timeForThisPlay = endTime - startTime;

  let bestTimeNumber = Number.isInteger(getBestTimeFromLocalStorage()) == true ? getBestTimeFromLocalStorage() : Number.MAX_SAFE_INTEGER;
  if (bestTimeNumber > timeForThisPlay) {
    localStorage.setItem('bestTime', timeForThisPlay);
  }

}

function updateBestScore(passedCurrentClicks) {

  if (passedCurrentClicks == null || passedCurrentClicks <= 0) {
    return;
  }


  let bestScoreNumber = Number.isInteger(getBestScoreFromLocalStorage()) == true ? getBestScoreFromLocalStorage() : Number.MAX_SAFE_INTEGER;
  if (bestScoreNumber > passedCurrentClicks) {
    localStorage.setItem('bestScore', passedCurrentClicks);
  }

}

function updateCurrentClicks() {

  document.getElementById("current-score").textContent = `${clickCounter}`;
}

function reRenderHighSocres() {

  if (Number.isInteger(getBestTimeFromLocalStorage()) == false) {

    document.getElementById("best-time").textContent = `Best Time Yet : -- seconds`;

  } else {
    let bestTimeNumber = getBestTimeFromLocalStorage();
    document.getElementById("best-time").textContent = `Best Time Yet : ${bestTimeNumber / 1000} seconds`;

  }

  if (Number.isInteger(getBestScoreFromLocalStorage()) == false) {

    document.getElementById("best-score").textContent = `Best Score Yet : -- clicks`;

  } else {
    let bestClicksNumber = getBestScoreFromLocalStorage();
    document.getElementById("best-score").textContent = `Best Score Yet : ${bestClicksNumber} clicks`;

  }
}

function afterConfirmation(resetIsConfirmed) {

  if (resetIsConfirmed === true) {

    document.getElementById("play").textContent = "Play";
    document.getElementById("play").setAttribute("title", "Click to start the game");
    toggleCurrentClicksDiv("none");
    diffcultyChangable = true;
    clickingIsAllowed = false;
    confirmationGivenForReset = null;

    shuffledCARDS = shuffle(CARDS.slice(CARDS.length - gameDiffculty[diffcultyLevel % 3]));
    removeDivsForGameArea();
    createDivsForCARDS(shuffledCARDS);

    previouslyClickedElement = null;
    clickCounter = 0;

    startTime = null;
    toggleInstructionsArea("flex");
    toggleConfirmResetMessageArea("none");

  } else if (resetIsConfirmed === false) {
    toggleConfirmResetMessageArea("none");


  }

}

